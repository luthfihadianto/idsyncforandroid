package com.ysi.idsyncclient;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.microsoft.aad.adal.AuthenticationCallback;
import com.microsoft.aad.adal.AuthenticationContext;
import com.microsoft.aad.adal.AuthenticationResult;
import com.microsoft.aad.adal.PromptBehavior;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.crypto.NoSuchPaddingException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
//    private static final String CLIENT_ID = "301f137d-682a-4830-b8e4-357f78335e3f";
//    private static final String REDIRECT_URI = "http://webapp/";
//    private static final String GRAPH_RESOURCE = "urn:api:ps";
//    private static final String AUTHORITY = "https://login.publicsector.id/adfs";

    private static final String LOG_TAG = "AUTH";
    private static final String LOG_IDSYNC_ERROR = "IDSYNC ERROR";
    private static final String LOG_IDSYNC = "IDSYNC LOG";

    private AuthenticationContext authenticationContext;

    ProgressDialog pDialog;
    TextView textView;
    TextView textViewData;
    Button buttonGetData;
    Button buttonLogOut;

    String _accessToken;
    int _expiredToken;

    String _userName;
    String _message ;

    SharedPreferences sharedPreferences;

    private static final String CLIENT_ID = "33dc2d30-abaa-4384-ab3d-f559175352c2";
    private static final String REDIRECT_URI = "http://webapiclient/";
    private static final String GRAPH_RESOURCE = "urn:webapi:bpn";
    private static final String AUTHORITY = "https://login.bpn.go.id/adfs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int dateTimeNowInt = (int) (new Date().getTime()/1000);
        Log.d(LOG_IDSYNC, "" + dateTimeNowInt);
        if (_accessToken == "" || _expiredToken <= dateTimeNowInt)
        {
            try {
                // Create the authentication context.
                authenticationContext = new AuthenticationContext(MainActivity.this,
                        AUTHORITY, false);

                // Acquire tokens using necessary UI.
                authenticationContext.acquireToken(MainActivity.this, GRAPH_RESOURCE, CLIENT_ID, REDIRECT_URI,
                        PromptBehavior.Always, new AuthenticationCallback<AuthenticationResult>() {
                            @Override
                            public void onSuccess(AuthenticationResult result) {
                                String idToken = result.getIdToken();
                                String accessToken = result.getAccessToken();

                                _accessToken = accessToken;

                                JWT jwt = new JWT(accessToken);
                                Claim claim = jwt.getClaim("email");
                                _userName = claim.asString();
                                _expiredToken = jwt.getClaim("exp").asInt();

                                //Save to sharedPreferences
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(getString(R.string.access_token), accessToken);
                                editor.putInt(getString(R.string.expired_token), _expiredToken);
                                editor.commit();

//                            _userName = String.valueOf(accessToken.split("\\.").length);
//                            _userName = result.getUserInfo().getDisplayableId();

                                // Print tokens.
                                Log.d(LOG_TAG, "ID Token: " + idToken);
                                Log.d(LOG_TAG, "Access Token: " + _accessToken);
                                Log.d(LOG_TAG, "Username: " + _userName);

                                textView.setText("Welcome, "  + _userName + "!");
                                buttonGetData.setVisibility(View.VISIBLE);
                                buttonLogOut.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Exception ex) {
                                Log.e(LOG_IDSYNC_ERROR, ex.getMessage());
                                // TODO: Handle error
                            }
                        });
            } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
                Log.e(LOG_IDSYNC_ERROR, ex.getMessage());
            }
        }
        else
        {
            JWT jwt = new JWT(_accessToken);
            _userName = jwt.getClaim("email").asString();
            textView.setText("Welcome, "  + _userName + "!");
            buttonGetData.setVisibility(View.VISIBLE);
            buttonLogOut.setVisibility(View.VISIBLE);
        }

        OkHttpClient okClient = new OkHttpClient();
        Request req = new Request.Builder()
                .url("https://api.bpn.go.id/api/v1/Ad/" + _userName + "/GetUserByUserPrincipalName")
                .header("Authorization", "Bearer " + _accessToken)
                .build();
        try {
            Response res = okClient.newCall(req).execute();
            JSONObject jsonObject = new JSONObject(res.body().string());
            _message = "";
            _message += "First Name : " + jsonObject.getString("FirstName") + "\n";
            _message += "Last Name : " + jsonObject.getString("LastName") + "\n";
            _message += "Job Title : " + jsonObject.getString("JobTitle") + "\n";
            _message += "Country : " + jsonObject.getString("Country") + "\n";
            _message += "City : " + jsonObject.getString("City") + "\n";
            _message += "Province : " + jsonObject.getString("Province") + "\n";
            _message += "PostalCode : " + jsonObject.getString("PostalCode") + "\n";
            _message += "Telephone : " + jsonObject.getString("Telephone") + "\n";
            _message += "Street : " + jsonObject.getString("Street") + "\n";
            _message += "User Principal Name : " + jsonObject.getString("UserPrincipalName") + "\n";
            _message += "Distinguished Name : " + jsonObject.getString("DistinguishedName") + "\n";
            _message += "Skype Provisioning : " + (jsonObject.getString("IsEnableSkype") == "1" ? "Enable" : "Disable") + "\n";
            _message += "Exchange Provisioning : " + (jsonObject.getString("IsEnableExchange") == "1" ? "Enable" : "Disable") + "\n";
            _message += "Sharepoint Provisioning : " + (jsonObject.getString("IsEnableSharepoint") == "1" ? "Enable" : "Disable") + "\n";
        } catch (IOException ex) {
            Log.e(LOG_IDSYNC_ERROR, ex.getMessage());
        } catch (JSONException ex) {
            Log.e(LOG_IDSYNC_ERROR, ex.getMessage());
        }


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        sharedPreferences = getPreferences(this.MODE_PRIVATE);
        _accessToken = sharedPreferences.getString(getString(R.string.access_token), "");
        _expiredToken = sharedPreferences.getInt(getString(R.string.expired_token), 0);

        //Initialize Component
        textView = (TextView) findViewById(R.id.textView);
        textViewData = (TextView) findViewById(R.id.textViewData);
        buttonLogOut = (Button) findViewById(R.id.buttonLogOut);
        buttonLogOut.setVisibility(View.INVISIBLE);
        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout();
            }
        });
        buttonGetData = (Button) findViewById(R.id.buttonGetData);
        buttonGetData.setVisibility(View.INVISIBLE);
        buttonGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetData().execute();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the authentication context.
        if (authenticationContext != null) {
            authenticationContext.onActivityResult(requestCode, resultCode, data);
        }
    }
    private class GetData extends AsyncTask<Void, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            OkHttpClient okClient = new OkHttpClient();
            Request req = new Request.Builder()
                    .url("https://api.bpn.go.id/api/v1/Ad/" + _userName + "/GetUserByUserPrincipalName")
//                    .url("https://api.bpn.go.id/api/Hello")
                    .header("Authorization", "Bearer " + _accessToken)
                    .build();
            try {
                Response res = okClient.newCall(req).execute();
//                _message = res.body().string();
                JSONObject jsonObject = new JSONObject(res.body().string());
                _message = "";
                _message += "First Name : " + jsonObject.getString("FirstName") + "\n";
                _message += "Last Name : " + jsonObject.getString("LastName") + "\n";
                _message += "Job Title : " + jsonObject.getString("JobTitle") + "\n";
                _message += "Country : " + jsonObject.getString("Country") + "\n";
                _message += "City : " + jsonObject.getString("City") + "\n";
                _message += "Province : " + jsonObject.getString("Province") + "\n";
                _message += "PostalCode : " + jsonObject.getString("PostalCode") + "\n";
                _message += "Telephone : " + jsonObject.getString("Telephone") + "\n";
                _message += "Street : " + jsonObject.getString("Street") + "\n";
                _message += "User Principal Name : " + jsonObject.getString("UserPrincipalName") + "\n";
                _message += "Distinguished Name : " + jsonObject.getString("DistinguishedName") + "\n";

                _message += "Skype Provisioning : " + (jsonObject.getString("IsEnableSkype") == "1" ? "Enable" : "Disable") + "\n";
                _message += "Exchange Provisioning : " + (jsonObject.getString("IsEnableExchange") == "1" ? "Enable" : "Disable") + "\n";
                _message += "Sharepoint Provisioning : " + (jsonObject.getString("IsEnableSharepoint") == "1" ? "Enable" : "Disable") + "\n";

            } catch (IOException ex) {
                Log.e(LOG_IDSYNC_ERROR, ex.getMessage());
            } catch (JSONException ex) {
                Log.e(LOG_IDSYNC_ERROR, ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            textViewData.setText(_message);

            if(pDialog.isShowing())
            { pDialog.dismiss(); }
        }
    }

    private void Logout()
    {
        OkHttpClient okClient = new OkHttpClient();
        Request req = new Request.Builder()
                .url("https://login.bpn.go.id/adfs/ls/?wa=wsignout1.0")
                .build();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.access_token), "");
        editor.putInt(getString(R.string.expired_token), 0);
        editor.commit();

        try {
            Response res = okClient.newCall(req).execute();
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } catch (IOException ex) {
            Log.e(LOG_IDSYNC_ERROR, ex.getMessage());
//                    e.printStackTrace();
        }
    }
}
